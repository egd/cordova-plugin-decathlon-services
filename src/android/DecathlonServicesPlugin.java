package com.decathlon.plugin.decathlonservices;

import com.decathlon.android.decathlonservices.available.AvailableManager;
import com.decathlon.android.decathlonservices.available.model.AvailableParcel;
import com.decathlon.android.decathlonservices.common.ServiceAvailableCallback;
import com.decathlon.android.decathlonservices.common.ServiceAvailableError;
import com.decathlon.android.decathlonservices.common.ServiceCallback;
import com.decathlon.android.decathlonservices.common.ServiceError;
import com.decathlon.android.decathlonservices.log.Log;
import com.decathlon.android.decathlonservices.login.LoginManager;
import com.decathlon.android.decathlonservices.login.model.*;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DecathlonServicesPlugin extends CordovaPlugin {

    //tags logs
    private static final String TAG = "Trocathlon";
    private static final String TAG2 = "STORES";
    private static final int MAX_STORES = 20;
    private LoginManager loginManager;
    private AvailableManager availableManager;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.initialise(cordova.getActivity().getApplicationContext());
    }

    public boolean execute(String action, final JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if ("authentication".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    authentication(callbackContext);
                }
            });

            return true;
        }

        if ("logout".equals(action)) {
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    logout(callbackContext);
                }
            });

            return true;
        }

        if ("checkState".equals(action)) {
            final String appId = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    available(callbackContext, appId);
                }
            });
            return true;
        }

        if ("log".equals(action)) {
            final String level = args.getString(0);
            final String log = args.getString(1);
            cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                    log(level, log);
                }
            });

            return true;
        }

        return false;
    }


    private void authentication(final CallbackContext callback) {
        final JSONObject authenticationResult = new JSONObject();

        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }
        loginManager.getUser(new ServiceCallback<User>() {
            @Override
            public void success(User user) {
                try {
                    JSONArray userStoreList = new JSONArray();
                    if (user.getUserStores() != null) {
                        int affectedStores = user.getUserStores().size();
                        int maxstores = MAX_STORES;
                        for (UserStore userStore : user.getUserStores()) {
                            JSONObject userStoredetail = new JSONObject();
                            userStoredetail.put("id", userStore.getId());
                            userStoredetail.put("name", userStore.getName());
                            userStoreList.put(userStoredetail);
                            Log.w(TAG2, userStoredetail.toString());
                            if ((--maxstores) < 0) break;
                        }
                    }
                    authenticationResult.put("token", loginManager.getCurrentToken());

                    authenticationResult.put("userStoreList", (Object) userStoreList);
                    authenticationResult.put("userId", user.getUserId());
                    authenticationResult.put("username", user.getName());
                    Log.i(TAG2, authenticationResult.toString());
                    callback.success(authenticationResult); // Thread-safe.
                } catch (
                        JSONException e)

                {
                    callback.error(e.getMessage());
                    Log.i("DecathlonServicesPlugin", e.getMessage());
                }
            }

            @Override
            public void failure(ServiceError serviceError) {
                Log.i("DecathlonServicesPlugin", serviceError.getErrorMessage());
                callback.error(serviceError.getErrorMessage());
            }
        });
    }

    private void logout(final CallbackContext callback) {
        if (loginManager == null) {
            loginManager = new LoginManager(cordova.getActivity().getApplicationContext());
        }

        loginManager.logout();
        Log.i("DecathlonServicesPlugin", "Process logout from Decathlon Services");
        callback.success();
    }

    private void available(final CallbackContext callback, final String appId) {
        if (availableManager == null) {
            availableManager = new AvailableManager(cordova.getActivity().getApplicationContext());
        }

        availableManager.getAvailableInformation(appId, new ServiceAvailableCallback<AvailableParcel>() {
            @Override
            public void success(AvailableParcel availableParcel) {
                try {
                    JSONObject json = new JSONObject();
                    json.put("state", availableParcel.getApkState());
                    json.put("version", availableParcel.getVersion());
                    callback.success(json);
                } catch (JSONException e) {
                    callback.error(e.getMessage());
                    Log.e("DecathlonServicesPlugin", e.getMessage());
                }
            }

            @Override
            public void failure(ServiceAvailableError serviceAvailableError) {
                Log.i("DecathlonServicesPlugin", serviceAvailableError.getErrorMessage());
                callback.error(serviceAvailableError.getErrorMessage());
            }
        });
    }

    public void log(final String level, final String log) {
        String levelLower = level.toLowerCase();

        if (levelLower.equals("error")) {
            Log.e(TAG, log);
        } else if (levelLower.equals("info")) {
            Log.i(TAG, log);
        } else if (levelLower.equals("debug")) {
            Log.d(TAG, log);
        } else if (levelLower.equals("verbose")) {
            Log.v(TAG, log);
        } else if (levelLower.equals("warn")) {
            Log.w(TAG, log);
        } else {
            Log.i(TAG, log);
        }
    }
}
