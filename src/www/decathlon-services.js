function DecathlonServices() {

}

DecathlonServices.prototype.authentication = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "authentication", []);
};

DecathlonServices.prototype.getUser = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "getUser", []);
};

DecathlonServices.prototype.logout = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "logout", []);
};

DecathlonServices.prototype.checkState = function(successCallback, errorCallback, appId) {
    cordova.exec(successCallback, errorCallback, "DecathlonServicesPlugin", "checkState", [appId]);
};

DecathlonServices.prototype.log = function(level, log) {
    cordova.exec(null, null, "DecathlonServicesPlugin", "logInfo", [level, log]);
};

module.exports = new DecathlonServices();